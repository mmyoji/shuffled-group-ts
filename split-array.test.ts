import { assert, assertEquals } from "std/testing/asserts.ts";

import { splitArray } from "./split-array.ts";

{
  const cases = [
    { arr: [1, 2, 3, 4, 5], parts: 0, expected: [[1, 2, 3, 4, 5]] },
    { arr: [1, 2, 3, 4, 5], parts: 1, expected: [[1, 2, 3, 4, 5]] },
    {
      arr: [1, 2, 3, 4, 5],
      parts: 2,
      expected: [
        [1, 2, 3],
        [4, 5],
      ],
    },
    { arr: [1, 2, 3, 4, 5], parts: 3, expected: [[1, 2], [3, 4], [5]] },
    { arr: [1, 2, 3, 4, 5], parts: 4, expected: [[1, 2], [3], [4], [5]] },
    { arr: [1, 2, 3, 4, 5], parts: 5, expected: [[1], [2], [3], [4], [5]] },

    { arr: [1, 2, 3, 4], parts: 1, expected: [[1, 2, 3, 4]] },
    {
      arr: [1, 2, 3, 4],
      parts: 2,
      expected: [
        [1, 2],
        [3, 4],
      ],
    },
    { arr: [1, 2, 3, 4], parts: 3, expected: [[1, 2], [3], [4]] },
    { arr: [1, 2, 3, 4], parts: 4, expected: [[1], [2], [3], [4]] },
  ];

  for (const { arr, parts, expected } of cases) {
    Deno.test(`splitArray() returns grouped arrays when arr=${arr} and parts=${parts}`, () => {
      const group = splitArray(parts)(arr);
      assertEquals(group, expected);
    });
  }
}

{
  const cases = [
    { arr: [1, 2, 3, 4, 5], parts: 6 },
    { arr: [1, 2, 3, 4], parts: 5 },
  ];

  for (const { arr, parts } of cases) {
    Deno.test(`splitArray() throws an error when parts=${parts} is greather than arr.length=${arr.length}`, () => {
      try {
        splitArray(parts)(arr);
        assert(false);
      } catch (err) {
        assertEquals(
          err.message,
          `parts must be less than or equal to given array size`,
        );
      }
    });
  }
}
