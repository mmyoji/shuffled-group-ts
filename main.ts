import { pipe } from "fp-ts/function";

import { shuffleArray } from "./shuffle-array.ts";
import { splitArray } from "./split-array.ts";

function randomGroup(members: string[], parts: number) {
  return pipe(members, shuffleArray(), splitArray(parts));
}

const arr = ["foo", "bar", "buz"];
console.log("Before:", arr);
console.log("After:", randomGroup(arr, 2));
