/**
 * @see https://javascript.info/task/shuffle
 */
export function shuffleArray<T>(): (arr: T[]) => T[] {
  return (arr) => {
    return arr.toSorted(() => Math.random() - 0.5);
  };
}
