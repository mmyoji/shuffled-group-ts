export function splitArray<T>(parts = 2): (arr: T[]) => T[][] {
  return (arr) => {
    if (parts < 2) {
      return [arr];
    }

    if (arr.length < parts) {
      throw new Error(`parts must be less than or equal to given array size`);
    }

    // ref: https://stackoverflow.com/a/51514813
    const tmp = [...arr];
    const result: T[][] = [];
    for (let i = parts; i > 0; i--) {
      result.push(tmp.splice(0, Math.ceil(tmp.length / i)));
    }
    return result;
  };
}
